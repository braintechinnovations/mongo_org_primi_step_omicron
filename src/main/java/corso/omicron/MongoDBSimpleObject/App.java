package corso.omicron.MongoDBSimpleObject;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class App 
{
    public static void main( String[] args )
    {

    	MongoClientURI connectionUri = new MongoClientURI("mongodb://localhost:27017");
    	
    	try {
    		
    		/*
    		 	{
				    _id: 12345,
				    "titolo": "La tempesta",
				    "autore": "W.S.",
				    "casa_editrice": {
				        "via": "Via degli olmi",
				        "citta": "Milano"
				    },
				    "categorie": ["Opera teatrale", "Classico letterario"],
				    "ristampe": [
				        {
				            "anno": 2010,
				            "formato": "Copertina flessibile"
				        },
				        {
				            "anno": 2012,
				            "formato": "Copertina rigida"
				        }
				    ]
				}
    		 */
    		MongoClient client = new MongoClient(connectionUri);
    		
    		MongoDatabase database = client.getDatabase("Libreria");
    		MongoCollection<Document> coll_libri = database.getCollection("archivio");
    		
    		// INSERIMENTO
//    		BasicDBList categorie = new BasicDBList();
//    		categorie.add("Opera teatrale");
//    		categorie.add("Classico letterario");
//    		
//    		BasicDBList ristampe = new BasicDBList();
//    		ristampe.add(new BasicDBObject("anno", 2010).append("formato", "Copertina flessibile"));
//    		ristampe.add(new BasicDBObject("anno", 2012).append("formato", "Copertina rigida"));
//    		
//    		Document libro = new Document("_id", 12346)
//    				.append("titolo", "La tempesta")
//    				.append("autore", "W.S.")
//    				.append("casa_editrice", new BasicDBObject("via", "via degli olmi").append("citta", "Milano"))
//    				.append("categorie", categorie)
//    				.append("ristampe", ristampe);
//    		
//    		coll_libri.insertOne(libro);
//    		
//    		System.out.println("Operazione effettuata con successo!");
    		
    		//Find First
//    		Document libroTemp = coll_libri.find().first();
//    		System.out.println(libroTemp.toJson());
    		
    		//Ricerca tramite FIlter
//    		Document libroTemp = coll_libri.find(Filters.eq("titolo", "La tempesta")).first();
//    		System.out.println(libroTemp.toJson());
    		
    		//Iterazione semplice
    		MongoCursor<Document> elencoRisultati = coll_libri.find().iterator();
    		
    		while(elencoRisultati.hasNext()) {
    			System.out.println(elencoRisultati.next().toJson());
    		}
    		
    		//TODO: Modifica
    		//TODO: Eliminazione
    		
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
		}
    	
    }
}
